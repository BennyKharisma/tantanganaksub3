<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Perusahaan;
use Faker\Generator as Faker;


$factory->define(Perusahaan::class, function (Faker $faker) {
    $events = App\Event::pluck('id')->toArray();
    return [
        'event_id' => $faker->randomElement($events),
        'nama_perusahaan' => $faker->company
    ];
});
