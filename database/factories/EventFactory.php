<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {
    return [
        'nama_event' => $faker->city,
        'tanggal_event' => $faker->dateTime
    ];
});
