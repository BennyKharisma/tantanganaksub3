<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Karyawan;
use Faker\Generator as Faker;

$factory->define(Karyawan::class, function (Faker $faker) {
    $company = App\Perusahaan::pluck('id')->toArray();
    return [
        'perusahaan_id' => $faker->randomElement($company),
        'nama' => $faker->name,
        'email' => $faker->email,
        'no_hp' => $faker->phoneNumber
    ];
});
