<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Event::class, 50)->create();
        factory(App\Perusahaan::class, 25)->create();
        factory(App\Karyawan::class, 30)->create();
    }
}
