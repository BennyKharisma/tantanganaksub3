<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use DB;

class EventController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }

    public function store(Request $request){
        Event::create([
            'nama_event' => $request->nama,
            'tanggal_event' => $request->tanggal
        ]);
        
        return response()->json([
            'message' => 'sukses input event',
        ]);
    }

        
    public function getPerusahaanData(){
        $dataPerusahaan = DB::table('events')
                          ->join('perusahaans', 'events.id', '=', 'perusahaans.event_id')
                          ->join('karyawans', 'perusahaans.id', '=', 'karyawans.perusahaan_id')
                          ->select('nama_event', 'nama_perusahaan', 'tanggal_event', 'nama', 'email', 'no_hp')
                          ->get();
        return response()->json([
            'data' => $dataPerusahaan
        ]);
    }
}
