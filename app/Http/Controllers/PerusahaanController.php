<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perusahaan;
use DB;

class PerusahaanController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }

    public function store(Request $request){
        Perusahaan::create([
            'event_id' => $request->event,
            'nama_perusahaan' => $request->nama
        ]);
        
        return response()->json([
            'message' => 'sukses input perusahaan',
        ]);
    }

    
    public function getKaryawanData(){
        $dataKaryawan = DB::table('perusahaans')
                        ->join('karyawans', 'perusahaans.id', '=', 'karyawans.perusahaan_id')
                        ->select('nama_perusahaan', 'nama', 'email', 'no_hp')
                        ->get();
        return response()->json([
            'data' => $dataKaryawan
        ]);
    }
}
