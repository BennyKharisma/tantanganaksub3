<?php

namespace App\Http\Controllers;

use App\Karyawan;
use Illuminate\Http\Request;
use DB;

class KaryawanController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }

    public function store(Request $request){
        karyawan::create([
            'perusahaan_id' => $request->perusahaan,
            'nama' => $request->nama,
            'email' => $request->email,
            'no_hp' => $request->no_hp,
        ]);
        
        return response()->json([
            'message' => 'sukses input karyawan',
        ]);
    }

    public function show($id){
        $karyawan = DB::table('karyawans')
                    ->join('perusahaans', 'perusahaans.id', '=', 'karyawans.perusahaan_id')
                    ->select('karyawans.id', 'nama_perusahaan','nama', 'email', 'no_hp')
                    ->where('karyawans.id', $id)
                    ->get();

        if($karyawan == null){
            return response()->json([
                'status' => 200,
                'data' => 'Tidak Ditemukan'
            ]);
        }
        return response()->json([
            'status' => 200,
            'data' => $karyawan
        ]);
    }

}
